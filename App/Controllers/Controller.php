<?php

class Controller extends App
{

    private function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    public function validate($entity)
    {

        if (strlen($entity->lname) < 2) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'last_name-error',
                'error_msg' => 'Введите Вашу фамилию',
                'asd' => $entity
            ]);
        }
        if (strlen($entity->fname) < 2) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'first_name-error',
                'error_msg' => 'Введите Ваше имя'
            ]);
        }
        if (!$this->validateEmail($entity->email)) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'email-error',
                'error_msg' => 'Пожалуйста, введите корректный email'
            ]);
        }
        if (strlen($entity->country) < 2) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'country-error',
                'error_msg' => 'Пожалуйста, выберите страну из выпадающего списка'
            ]);
        }
        if (strlen($entity->gender) < 2) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'gender-error',
                'error_msg' => 'Пожалуйста, выберите пол'
            ]);
        }
        if (strlen($entity->birth_date) < 2) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'birth_date-error',
                'error_msg' => 'Пожалуйста, введите день рождения'
            ]);
        }
        if (!$entity->agree) {
            return json_encode([
                'success' => 0,
                'error_block_id' => 'agree-error',
                'error_msg' => 'Пожалуйста, примите условия'
            ]);
        }

        return json_encode([
            'success' => 1,
            'error_msg' => ''
        ]);
    }

    public function renderLayout($body)
    {
        ob_start();
        require $_SERVER['DOCUMENT_ROOT'].'/App/Views/Layouts/Layout.php';
        return ob_get_clean();
    }

    public function render($viewName)
    {
        $viewFile = $_SERVER['DOCUMENT_ROOT'].'/App/Views/'.$viewName.'.php';
        ob_start();
        require $viewFile;
        $body = ob_get_clean();
        ob_end_clean();
        return $this->renderLayout($body);
    }

    public function index()
    {
        return $this->render('Index');
    }

}

?>