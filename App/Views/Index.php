<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="infocontrol-form-wrapper">
                <h1 class="title">Infokontrol</h1>
                <form data-ed="infocontrol-form">
                    <div class="form-group">
                        <div class="col-md-6 nopaddingl">
                            <input type="text" class="form-control" placeholder="Ваша фамилия" id="last_name"
                                   name="last_name">
                            <div class="ed-error-wrapper hw" id="last_name-error"></div>
                        </div>
                        <div class="col-md-6 nopaddingr">
                            <input type="text" class="form-control" placeholder="Ваше имя" id="first_name"
                                   name="first_name">
                            <div class="ed-error-wrapper hw" id="first_name-error"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Ваш email" id="email" name="email">
                        <div class="ed-error-wrapper" id="email-error"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" id="country" name="country">
                        <select class="form-control" id="country-selector">
                            <option></option>
                            <?php
                                $ch = curl_init("https://namaztimes.kz/ru/api/country");
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $result = json_decode(curl_exec($ch));
                                curl_close($ch);
                                foreach ($result as $entity) {
                                    echo '<option value="'.$entity.'">'.$entity.'</option>';
                                }
                            ?>
                        </select>
                        <div class="ed-error-wrapper" id="country-error"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" id="gender" name="gender">
                        <div class="col-md-6 nopaddingl">
                            <input class="form-check-input" type="radio" id="genderman" value="Мужчина" checked>
                            <label class="form-check-label" for="genderman">
                                Мужчина
                            </label>
                        </div>
                        <div class="col-md-6 nopaddingr">
                            <input class="form-check-input" type="radio" id="genderwoman" value="Женщина">
                            <label class="form-check-label" for="genderwoman">
                                Женщина
                            </label>
                        </div>
                        <div class="ed-error-wrapper" id="gender-error"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" id="birth_date" name="birth_date">
                        <div class='input-group date' id="datetimepicker3">
                            <input type="text" class="form-control"/>
                            <span class="input-group-addon">
                           <span class="glyphicon glyphicon-time"></span>
                           </span>
                        </div>
                        <div class="ed-error-wrapper" id="birth_date-error"></div>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="agree">
                        <label class="form-check-label" for="agree">
                            Согласен с условиями
                        </label>
                        <div class="ed-error-wrapper" id="agree-error"></div>
                    </div>

                    <div class="form-group mt-15">
                        <button type="submit" class="btn btn-primary mb-2">Создать аккаунт</button>
                    </div>
                    <div class="form-group">
                        <p class="infocontrol-form-result" data-ed="infocontrol-form-result"></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>