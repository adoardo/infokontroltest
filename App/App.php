<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/App/Controllers/Controller.php';

class App
{
    private $method;
    private $uri;
    private $action;
    private $index;

    public function __construct()
    {

        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

        if (count($this->uri) > 1) {
            $this->action = $this->uri[2];
            $tmp = explode('?', $this->action);
            if (count($tmp) > 0) {
                $this->action = $tmp[0];
            }
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
        } else {
            $this->action = 'index';
        }

    }

    private function validateRequest($method, $action)
    {
        if ($method == 'GET' && ($action == 'validate' || $action == 'index')) {
            return true;
        } else {
            return false;
        }
    }

    public function run()
    {

        if (!$this->validateRequest($this->method, $this->action)) {
            return json_encode([
                'error' => [
                    'code' => 400,
                    'message' => 'The request was invalid.'
                ]
            ]);
        }

        $data = new stdClass();
        $data->fname = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : null;
        $data->lname = isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : null;
        $data->email = isset($_REQUEST['email']) ? $_REQUEST['email'] : null;
        $data->country = isset($_REQUEST['country']) ? $_REQUEST['country'] : null;
        $data->gender = isset($_REQUEST['gender']) ? $_REQUEST['gender'] : null;
        $data->birth_date = isset($_REQUEST['birth_date']) ? $_REQUEST['birth_date'] : null;
        $data->agree = isset($_REQUEST['agree']) ? $_REQUEST['agree'] : null;

        $obj = new Controller();

        return $obj->{$this->action}($data);

    }

}

?>