document.addEventListener("DOMContentLoaded", function () {
    $('#country-selector').select2({
        placeholder: "Выберите страну",
        allowClear: true
    }).on('change', function () {
        $('input#country').val($(this).val());
        if ($(this).val()) {
            $('#country-error').css({
                display: 'none'
            });
        }
    });

    moment.locale('ru');
    $('#datetimepicker3').datetimepicker({
        format: 'DD.MM.YYYY',
        locale: 'ru'
    }).on('dp.change', function (e) {
        var myDate = moment(e.date).format('DD.MM.YYYY');
        myDate = myDate.split(".");
        var newDate = new Date(myDate[2], myDate[1] - 1, myDate[0]);
        console.log(newDate.getTime());
        $('input#birth_date').val(newDate.getTime());
        $('#birth_date-error').css({
            display: 'none'
        });
    });

    $('#datetimepicker3 .form-control').on('click', function () {
        $('#datetimepicker3').find('.input-group-addon').click();
    });

    $('#agree').on('change', function() {
        if ($(this).is(":checked")) {
            $('#agree-error').css({
                display: 'none'
            });
        }
    });

    $('#last_name, #first_name, #email').on('input', function () {
        target = $(this).attr('id');
        if ($(this).val().length > 2) {
            $('#' + target + '-error').css({
                display: 'none'
            });
        }
    });

    $('[data-ed="infocontrol-form"]').submit(function (e) {
        $('.ed-error-wrapper').css({
            display: 'none'
        });
        e.preventDefault();

        if ($('#genderman').is(":checked")) {
            $('#gender').val($('#genderman').val());
        }
        if ($('#genderwoman').is(":checked")) {
            $('#gender').val($('#genderwoman').val());
        }
        //Валидируем поля
        if ($('#agree').is(":checked")) {
            ag = 1;
        } else {
            ag = 0;
        }
        myData = {
            last_name: $('#last_name').val(),
            first_name: $('#first_name').val(),
            email: $('#email').val(),
            gender: $('#gender').val(),
            country: $('#country').val(),
            birth_date: $('#birth_date').val(),
            agree: ag
        };
        $.ajax({
            type: "GET",
            url: '/api/v1/validate',
            data: myData,
            success: function (response) {
                console.log(response);

                if (response.success == 1) {

                    myData = {
                        last_name: $('#last_name').val(),
                        first_name: $('#first_name').val(),
                        email: $('#email').val(),
                        gender: $('#gender').val(),
                        country: $('#country').val(),
                        birth_date: $('#birth_date').val(),
                        agree: true
                    };
                    $.ajax({
                        type: "POST",
                        url: 'https://test-api.infokontrol.com/playground',
                        data: JSON.stringify(myData),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data === 'error') {
                                $('[data-ed="infocontrol-form-result"]').html('Ошибка отправки данных').css({
                                    background: '#d73e3e'
                                });
                            } else if (data === 'success') {
                                $('[data-ed="infocontrol-form-result"]').html('Данные успешно отправлены').css({
                                    background: '#25c025'
                                });
                            }
                        },
                        error: function (data) {
                            console.log(data.responseText);
                        }
                    });

                } else {
                    $('#' + response.error_block_id).html('<p>' + response.error_msg + '</p>').css({
                        display: 'flex'
                    });
                }
            },
            error: function (e) {
                console.log(e.responseText);
                alert('Непредвиденная ошибка ' + e.responseText);
            },
            dataType: 'json'
        });
    });

});