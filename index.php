<?php

require_once __DIR__ .'/App/App.php';

try {
    $app = new App();
    echo $app->run();
} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}

?>